#!/bin/bash

source ./.lib/log.bash

install composer composer
install php php

mkdir -p ~/.local/bin/twig

composer require --working-dir=$HOME/.local/bin/twig friendsoftwig/twigcs