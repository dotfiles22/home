#!/bin/bash

source ./.lib/log.bash

install bat bat

link_config $(pwd)/term/config/bat ~/.config/bat

cd ~/.config/bat; bash install.sh