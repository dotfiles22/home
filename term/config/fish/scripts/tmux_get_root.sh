#!/bin/bash

if [[ -z "${TMUX}" ]]; then
  echo $ROOT_DIR
else 
  tmpfile=$(mktemp)
  tmux run "printf '#{session_path}\n' > \"$tmpfile\""
  cat $tmpfile
  rm $tmpfile
fi