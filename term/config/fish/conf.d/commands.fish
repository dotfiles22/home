
# list -> l

if command -v eza >/dev/null
    alias list='eza --icons --header --git-repos --git --time-style="+%H:%M:%S %d/%m/%Y" -x'
else if command -v exa >/dev/null
    alias list='exa --icons --git'
else
    alias list='ls'
end

alias l='list'
alias ll='list -l'
alias la='list -a'
alias lla='list -la'

# change -> c

alias c="cd"

# show/see -> s
alias s='bat'

# edit -> e
alias e='$EDITOR'

alias root='cd (tmux_get_root.sh)'

if command -v nap >/dev/null
    alias snip="NAP_CONFIG=$HOME/.config/nap/config.yaml NAP_HOME=$HOME/.config/nap-data nap"
end

if command -v rip >/dev/null
    alias rm='rip'
end

if command -v docker >/dev/null
    alias container='docker'
    alias compose='docker-compose'
else if command -v podman >/dev/null
    alias container='podman'
    alias compose='podman-compose'
end

alias localconfig='$EDITOR ~/.config/localfish/config.fish'