mkdir -p ~/.local/bin/go
set -gx GOBIN ~/.local/bin/go
fish_add_path ~/.local/bin/go

fish_add_path ~/.config/fish/scripts
set -x EDITOR hx