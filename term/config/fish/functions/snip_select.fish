function snip_select
    skate set snip (find ~/.config/snip/snippets/ -type f -printf '%P\n' | fzf --preview 'bat --color=always --style=numbers ~/.config/snip/snippets/{}')
end
