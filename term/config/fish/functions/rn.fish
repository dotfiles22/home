function rn
	if [ $argv[1] ]
		set filename $argv[1]
		set new_filename (gum input --value=$filename --header='New filename')
		mv "$filename" "$new_filename"
		gum format "`$filename` renamed to `$new_filename`"
	else
		echo "Usage :"
		echo "rn filename"
	end
end
