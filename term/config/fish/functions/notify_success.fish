function notify_success
    if [ $argv[1] ]
       echo -n "$argv[1]" > ~/.local/tmp/notify-latest
        echo "[Success] $argv[1]" >> ~/.local/tmp/notifications
        echo "----------" >> ~/.local/tmp/notifications
        tmux split-window -l 1 -b -d 'echo -n " "; set_color -b green; set_color -o white; echo -n " Success "; set_color normal; echo -n " "; cat ~/.local/tmp/notify-latest; sleep 4'
    end
end
