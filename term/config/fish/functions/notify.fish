
function notify
    if [ $argv[1] ]
        mkdir -p ~/.local/tmp
        echo -n $argv[1] > ~/.local/tmp/notify-latest
        echo $argv[1] >> ~/.local/tmp/notifications
        echo "----------" >> ~/.local/tmp/notifications
        tmux display-message (cat ~/.local/tmp/notify-latest) &
        # tmux popup -x9999 -y 0 -w10% -h5 -b padded -E -T 'title' 'cat ~/.local/tmp/notify-latest; sleep 4'
    end
end
