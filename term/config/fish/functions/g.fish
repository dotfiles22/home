function g
    set commands issue branch commit graph add diff mr show 
    
 	if [ $argv[1] ]
	   set command $argv[1]
    else
        set command (printf '%s\n' $commands | gum filter --header="Git" --height=10)
    end

    switch $command
        case branch
            set -l branch_actions create delete switch
         	if [ $argv[2] ]
        	   set sub_command $argv[2]
            else
               set sub_command (printf '%s\n' $branch_actions | gum filter --header="Git branch" --height=10)
            end

            switch $sub_command
                case create
                    git switch -c (gum input)
                case delete
                case switch
                    set selected_branch (git branch -a | cut -f 1 | sed 's|remotes/origin/||' | tr -d '\t *' | gum filter)
                    git switch $selected_branch
            end
        
        case issue
            set -l issue_actions show select new close note
         	if [ $argv[2] ]
        	   set sub_command $argv[2]
            else
               set sub_command (printf '%s\n' $issue_actions | gum filter --header="Git issue" --height=10)
            end
        
            switch $sub_command
                case new 
                    # Todo implementer une interface avec gum
                    glab issue create 2>/dev/null
                case close
                    if gum confirm "Fermer le ticket #$ISSUE_ID ?"
                        glab issue close $ISSUE_ID 2>/dev/null
                    end
                case select
                    set issues_with_title (glab issue list 2>/dev/null | tail -n +3 | cut --fields=1,3)
                    set -p --universal issues_with_title (printf '%s\t%s\n' 'None' 'Select none')
                    set --universal ISSUE_ID (printf '%s\n' $issues_with_title | fzf --preview='glab issue show (echo {} | cut --fields=1) 2>/dev/null' | cut --fields=1 | tr -d '#')
                    echo $ISSUE_ID
                    if test "$ISSUE_ID" = 'None'
                        set --universal ISSUE_ID ''
                    end
                case show
                    if test "$ISSUE_ID" != ''
                        glab issue show --comments $issue_id
                    else
                        echo "No issue selected"
                    end
            end

        
        case commit
            set -l TYPE (gum choose "fix" "feat" "docs" "refactor" "chore" "ci")
            set -l SCOPE (gum input --placeholder "scope")

            # Since the scope is optional, wrap it in parentheses if it has a value.
            test -n "$SCOPE" && set -l SCOPE "($SCOPE)"

            # Pre-populate the input with the type(scope): so that the user may change it
            set -l ISSUE ""
            if test "$ISSUE_ID" != "" 
                set ISSUE "#$ISSUE_ID - "
            end

            set -l SUMMARY (gum input --value "$TYPE$SCOPE: $ISSUE" --placeholder "Sommaire")
            set -l DESCRIPTION (gum write --placeholder "Details (CTRL+D si fini)")

            # Commit these changes
            echo $SUMMARY
            echo $DESCRIPTION
            gum confirm "On commit ?" && git commit -m "$SUMMARY" -m "$DESCRIPTION"

        case history
            git log --patch $argv[2]

        case graph
          git log --graph --pretty=tformat:'%C(red)%h%C(yellow)%d%Creset %s %Cgreen%ad %C(bold blue)%aN%Creset' --abbrev-commit --date=human
    end
end