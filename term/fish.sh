#!/bin/bash

source ./.lib/log.bash

install starship
install fish

link_config $(pwd)/term/config/fish ~/.config/fish
