#!/bin/bash

function log() {
  if test "$LOG" = 'debug'; then
    echo "[LOG] " $1
  fi
}

function check_command() {
  if ! command -v $1 1>/dev/null; then
    return 1
  fi
  return 0
}

function install() {
  to_install=$1
  bin_to_check=$2

  if test ! -z "$bin_to_check" && check_command "$bin_to_check"; then
    if ! gum confirm "$(echo -e "# $to_install is already installed\n> Do you want to reinstall it ?" | gum format)"; then 
      echo "Cancelled."
      return 0
    fi
  fi
 
  gum spin --title "Installing $to_install" -- nix-env -i $to_install
  echo -e -n "\r"
  gum format "> $to_install done"
}

function link_config() {
  config_path=$1
  link_path=$2

  mkdir -p ~/.config/
  log "Adding $link_path configuration"
  
  if [[ -L "$link_path" ]]; then
	  unlink "$link_path"
  fi

  if [ -d "$link_path" ]; then
    rm -rf $link_path
  fi

  ln -s $config_path $link_path
}
