#!/bin/bash

source ./.lib/log.bash

install neovim nvim

link_config $(pwd)/tui/config/nvim ~/.config/nvim
