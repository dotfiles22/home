return function(c)
  local r = vim.api.nvim_win_get_cursor(0)[1]
  vim.api.nvim_set_cursor(0, {r,c})
end