return function ()
  local r,c = unpack(vim.api.nvim_win_get_cursor(0))
  vim.api.nvim_set_cursor(0, {r+1,c})
end