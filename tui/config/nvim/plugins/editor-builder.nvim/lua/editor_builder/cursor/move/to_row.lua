return function(r)
  local c = vim.api.nvim_win_get_cursor(0)[2]
  vim.api.nvim_set_cursor(0, {r,c})
end