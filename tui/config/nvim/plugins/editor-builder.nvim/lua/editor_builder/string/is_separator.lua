return function(character)
	-- local separators = vim.api.nvim_buf_get_option(0, 'iskeyword')	

	local ns = {}
	ns.id = vim.api.nvim_create_namespace("helix")

	-- local src = vim.new_highlight_source()
	-- local buf = vim.current.buffer

	for _, i in ipairs({0,1,2,3,4}) do
		vim.api.nvim_buf_add_highlight(0, ns.id, "CurSearch", i, 0, -1)
	end
end
