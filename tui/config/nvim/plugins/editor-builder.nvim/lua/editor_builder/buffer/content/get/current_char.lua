return function()
  local c = (vim.api.nvim_win_get_cursor(0)[2] + 1)
  local content = vim.api.nvim_get_current_line()
  return string.sub(content, c, c)
end