local Commands = {}

Commands.load = function ()
	helix.cmd = {}

	-- helix.cmd.move_*
	require('helix-cmd.move')
	require('helix-cmd.extend')

	helix.cmd.goto_line_start = function ()
		require('editor_builder.cursor.move.to_col')(0)
	end

	-- In development
	-- helix.cmd.move_next_word_start = function ()
	-- 	local line_pos, col_pos = unpack(vim.api.nvim_win_get_cursor(0));
	-- 	line = vim.api.nvim_get_current_line();

	-- 	-- Functions
	-- 	local is_text = function(character)
	-- 		if character == '' then
	-- 			return false
	-- 		end
	-- 		result = string.find("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", character) 
	-- 		if result ~= nil then
	-- 			return true
	-- 		end
	-- 		return false
	-- 	end

	-- 	local get_char_at_index = function (line, index)
	-- 		return string.sub(line, index + 1, index + 1) 
	-- 	end

	-- 	local move_right = function ()
	-- 		vim.api.nvim_feedkeys('l', 'n', false)
	-- 	end

	-- 	local exit_visual = function()
	-- 		escape = vim.api.nvim_replace_termcodes('<esc>', true, false, true)
	-- 		vim.api.nvim_feedkeys(escape, 'n', false)
	-- 	end

	-- 	local enter_visual = function()
	-- 		vim.api.nvim_feedkeys('v', 'n', false)
	-- 	end

	-- 	-- Create a new Visual mode selection
	-- 	local mode = vim.fn.mode()
	-- 	print('current mode : [' .. mode .. ']')
	-- 	if mode == 's' then
	-- 		exit_visual()
	-- 	end
	-- 	enter_visual()

	-- 	-- Behavior
	-- 	print(get_char_at_index(line, col_pos))
	-- 	if is_text(get_char_at_index(line, col_pos)) == false then
	-- 		print('moved from index: ' .. col_pos)
	-- 		exit_visual()
	-- 		move_right()
	-- 		col_pos = col_pos + 1
	-- 		enter_visual()
	-- 	end
		
	-- 	print('--- start ---')
	-- 	print('line: [' .. line .. ']')
	-- 	local continue = true
	-- 	while continue do
	-- 		current_char = get_char_at_index(line, col_pos)
	-- 		print('current: [' .. current_char .. ']')

	-- 		if current_char ~= '' and is_text(current_char) == false then
	-- 			print('is_text: false')
	-- 			continue = false
	-- 		elseif current_char ~= '' then
	-- 			move_right()
	-- 			col_pos = col_pos + 1
	-- 			print('col_pos:' .. col_pos)
	-- 			print('is_text: true')
	-- 		end
	-- 		print('--- endloop')
	-- 	end

	-- 	print('--- end ---')
	-- end

	helix.cmd.delete_selection = require('helix-cmd.delete_selection')
	helix.cmd.toggle_comments = require('helix-cmd.code.toggle_comments')

	-- Picker
	
	helix.cmd.file_picker_in_current_directory = require('helix-cmd.picker.file_picker_in_current_directory')
	helix.cmd.symbol_picker = require('helix-cmd.picker.symbol_picker')
	helix.cmd.workspace_symbol_picker = require('helix-cmd.picker.workspace_symbol_picker')
end

return Commands
