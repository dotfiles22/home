local cmd_name = 'extend_line_below'
return function()
	print('--- extend_line_below')
	local ns = helix.var.ns.id
  local hi = helix.var.ns.highlight.select
  local row, column = unpack(vim.api.nvim_win_get_cursor(0))

	local line_content = vim.api.nvim_get_current_line()
	local next_line = false
	if line_content == '' then
		row = row + 1
		next_line = true
	end

	print('row: ' .. row .. 'col : ' .. column)
	line_content = vim.api.nvim_buf_get_lines(0, row - 1, row, true)[1]
	print('content: '  .. line_content)
	vim.api.normal_selection.select({row - 1, string.len(line_content)}, false)
	-- vim.api.nvim_buf_set_extmark(0, ns, row, 0, {}
	-- vim.api.nvim_buf_add_highlight(0, ns, "IncSearch", row-1, 0, -1)
	-- vim.api.nvim_win_set_cursor(0, {row, string.len(line_content)})

	vim.api.nvim_buf_set_var(0, 'select_active', true)
	vim.api.nvim_buf_set_var(0, 'select_active_motion', 'extend_line_below')
	-- TODO: Add lines bellow if already in NormalSelection
	print('--- end extend_line_below')
end
