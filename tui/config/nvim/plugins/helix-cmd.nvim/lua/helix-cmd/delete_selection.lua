-- Utils
local remove_char_in_string = function(string, index)
  return string.sub(string, 0, index) .. string.sub(string, index+2, -1)
end

local get_char_in_string = function (string, index)
  return string.sub(string, index, index)
end 

-- Behavior function
local is_oob = function (line, cursor_col)
  local character = get_char_in_string(line, cursor_col + 1)

  if character == '' then
    return true
  end
  return false
end

-- Command
--
-- Cases managed :
-- Case 1. Cursor is positionned on content : Remove the caracter under cursor
-- Case 2. We are oob : Remove the newline character
-- Case 3. Buffer is empty : Do nothing 
-- BUG : Copy paste the content bellow uncommented
--
-- some text
--
--             line
-- If your remove character between thoose two lines it, content will be wrong
return function()
  local content = vim.api.nvim_get_current_line()
  local r, c = unpack(vim.api.nvim_win_get_cursor(0))
  -- Out of bound, meaning the cursor is not on actual text
  local oob = is_oob(content, c)

	-- Case 3
	if oob and vim.api.nvim_buf_line_count(0) == 1 then
    return
  end
 
	-- Case 2
	if oob then
		last_character = get_char_in_string(content, #content)
		-- Join lines
		vim.cmd(r ..'j')
		-- Reposition cursor
		vim.api.nvim_win_set_cursor(0, {r, c})

		-- joining adds a space if the last character is not a space. Not an helix behavior
		if last_character ~= ' ' and content ~= '' then
			-- Update content var
			content = vim.api.nvim_get_current_line()
			content = remove_char_in_string(content, c)
			vim.api.nvim_set_current_line(content)
		end
		return
		end

  -- Case 3
  content = remove_char_in_string(content, c)
  vim.api.nvim_set_current_line(content)
end
