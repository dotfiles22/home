local Plugins = {
	{
		dir = vim.fn.expand('$HOME/.config/nvim/plugins/helix.nvim/'),
		dependencies = {
			{dir = vim.fn.expand('$HOME/.config/nvim/plugins/editor-builder.nvim/')},
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope.nvim",
			"numToStr/Comment.nvim",
			"lukas-reineke/indent-blankline.nvim",
			"lewis6991/gitsigns.nvim",
			"folke/todo-comments.nvim"
		},
		opts = {},
	},
	{	dir = vim.fn.expand('$HOME/.config/nvim/plugins/helix-cmd.nvim/')},
  {'kyazdani42/nvim-web-devicons', lazy = true},
	{ "catppuccin/nvim", name = "catppuccin", priority = 1000 },
	{
	"folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
		end,
		opts = {
			-- your configuration comes here
			-- or leave it empty to use the default settings
			-- refer to the configuration section below
		}
	}
}

return Plugins
