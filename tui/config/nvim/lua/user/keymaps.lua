local map = function (mode, mapping, action)
  vim.keymap.set(mode, mapping, action, {
    noremap = true,
  })
end
-- Space as leader key vim.g.mapleader = ' '
vim.g.mapleader = ' '
-- Shortcuts
vim.keymap.set("n", 'H', ':bprev<CR>')
vim.keymap.set("n", 'L', ':bnext<CR>')

-- Disable extended command mode mappings
-- BUG extended search goes to command
-- BUG inverted extended search goes to command
vim.api.nvim_create_autocmd({"CmdwinEnter"}, {
    callback = function()
      vim.cmd(':q')
      vim.api.nvim_feedkeys(':', 'n', false)
    end
})
