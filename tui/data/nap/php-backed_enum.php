<?php

enum Suit: string
{
    case Hearts = 'H';
    case Diamonds = 'D';
    case Clubs = 'C';
    case Spades = 'S';
}

print Suit::Clubs->value; // displays 'C'
print Suit::Clubs->name; // displays 'Clubs'
